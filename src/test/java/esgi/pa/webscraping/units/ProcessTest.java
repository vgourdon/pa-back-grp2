package esgi.pa.webscraping.units;

import esgi.pa.webscraping.mockAPI.MockAzureQueue;
import esgi.pa.webscraping.mockAPI.MockYoutubeSearch;
import esgi.pa.webscraping.models.Search;
import esgi.pa.webscraping.models.StateSearch;
import esgi.pa.webscraping.models.Video;
import esgi.pa.webscraping.shared.Utils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class ProcessTest {
    private final String SEND_QUEUE = "gr2-tasks-queue-send-usertest";
    private final String RECEIVE_QUEUE = "gr2-tasks-queue-receive-usertest";

    private Video video;
    private Search search;
    private MockAzureQueue azureQueue;
    private MockYoutubeSearch youtubeSearch;

    @BeforeEach
    public void setup() throws MalformedURLException {
        azureQueue = new MockAzureQueue();
        youtubeSearch = new MockYoutubeSearch();

        URL urlSearch = new URL("https://www.youtube.com/watch?v=M8oaSEn5xug");
        URL urlVideo = new URL("https://www.youtube.com/watch?v=M8oaSEn5xug");
        URL urlThumbnails = new URL("https://i.ytimg.com/vi/M8oaSEn5xug/maxresdefault.jpg");

        search = new Search(urlSearch.toString(),
                    "Gaming",
                    "Dahmien7",
                        "escapefromtarkov, eft",
                        "usertest", 1, 1);

        video = new Video(urlVideo, "HOLD UP SUR DOUANES !!", "Dahmien7", urlThumbnails, search);
    }

    @Test
    public void should_azure_send_queue_be_empty() {
        assertEquals(0, azureQueue.getQueueSize(SEND_QUEUE));
    }

    @Test
    public void should_azure_send_queue_has_a_size_equals_to_one_after_added_a_message() {
        azureQueue.postAzureMessageReceive("{\"urlVideo\":\"https://www.youtube.com/watch?v=M8oaSEn5xug\"," +
                                                            "\"categoryVideo\":\"Gaming\"," +
                                                            "\"youtuberName\":\"dahmien7\"," +
                                                            "\"keyWords\":\"eft, escapefromtarkov, dahmien7\"," +
                                                            "\"username\":\"usertest\"," +
                                                            "\"idSearch\":158," +
                                                            "\"idUser\":1}",
                                                        SEND_QUEUE);
        assertEquals(1, azureQueue.getQueueSize(SEND_QUEUE));
    }

    @Test
    public void should_azure_queue_be_empty_after_getting_a_message() {
        azureQueue.postAzureMessageReceive("test", SEND_QUEUE);
        azureQueue.getAzureMessage(SEND_QUEUE);
        assertEquals(0, azureQueue.getQueueSize(SEND_QUEUE));
    }

    @Test
    public void should_get_the_same_message_after_sending_and_receiving_it() {
        String message = "test";
        azureQueue.postAzureMessageReceive(message, SEND_QUEUE);
        assertEquals(message, azureQueue.getAzureMessage(SEND_QUEUE));
    }

    @Test
    public void should_get_all_infos_about_a_video() throws MalformedURLException {
        Video tmpVideo = youtubeSearch.getInfosForYTVideo(search.getUrlVideo(), search, null, true);

        Video newTmpVideo = new Video(new URL(search.getUrlVideo()),
                "Gaming",
                "Dahmien7",
                "escapefromtarkov, eft",
                "usertest",
                new URL("https://i.ytimg.com/vi/M8oaSEn5xug/maxresdefault.jpg"),
                search);
        assertEquals(newTmpVideo, tmpVideo);
    }

    @Test
    public void should_get_infos_added_for_a_video() throws MalformedURLException {
        Video tmpVideo = youtubeSearch.getInfosForYTVideo("https://www.youtube.com/watch?v=M8oaSEn5xug",
                search,
                video,
                                                false);

        video.setCategories("Gaming");
        video.setTags("escapefromtarkov, eft");
        assertEquals(video, tmpVideo);
    }

    @Test
    public void should_return_ten_video_from_a_string_param() throws MalformedURLException {
        ArrayList<Video> tmpArray = youtubeSearch.searchVideos("dahmien7", search);
        assertEquals(10, tmpArray.size());
    }

    @Test
    public void should_calculate_the_score_for_a_video() {
        video.setCategories("Gaming");
        video.setTags("escapefromtarkov, eft");
        video.setScore();
        assertEquals(40.0, (double) video.getScore());
    }

    @Test
    public void should_be_null_the_search_state() {
        assertNull(search.getStateSearch());
    }

    @Test
    public void should_be_created_and_set_to_ten_the_search_state() {
        search.setStateSearch(new StateSearch(search.getIdSearch(), search.getIdUser(), Utils.States.STEP_10));
        assertEquals(Utils.States.STEP_10, search.getStateSearch().getState());
    }

    @Test
    public void should_be_updated_and_equals_twenty_the_search_state() {
        search.setStateSearch(new StateSearch(search.getIdSearch(), search.getIdUser(), Utils.States.STEP_10));
        search.updateStateSearch(Utils.States.STEP_20);
        assertEquals(Utils.States.STEP_20, search.getStateSearch().getState());
    }

    @Test
    public void should_get_size_queue_equals_to_one_after_sending_results_to_the_queue() {
        azureQueue.postAzureMessageReceive("Video :" +
                                                            "urlVideo = https://www.youtube.com/watch?v=W0Auks3LgEo" +
                                                            "title = LA COURSE POURSUITE DES ENFERS..." +
                                                            "channel = dahmien7" +
                                                            "category = Gaming" +
                                                            "tags = escapefromtarkov, eft, dahmien7" +
                                                            "thumbnails = https://i.ytimg.com/vi/W0Auks3LgEo/default.jpg" +
                                                            "idSearch = 1" +
                                                            "score = 30.0" +
                                                        "Video :" +
                                                            "urlVideo = https://www.youtube.com/watch?v=_K8Q_dzZ8sk" +
                                                            "title = NEW RECORD PERSO SOLO - dahmien7" +
                                                            "channel = dahmien7" +
                                                            "category = Gaming" +
                                                            "tags = PUBG, FR, DAHMIEN7, playerunknown&#39;s battlegrounds, HIGHLIGHTS" +
                                                            "thumbnails = https://i.ytimg.com/vi/_K8Q_dzZ8sk/default.jpg" +
                                                            "idSearch = 1" +
                                                            "score = 20.0" +
                                                        "Video :" +
                                                            "urlVideo = https://www.youtube.com/watch?v=Oru2Sebkm-s" +
                                                            "title = JE DEVIENS RICHE EN TUANT CRESUS !" +
                                                            "        channel = dahmien7" +
                                                            "category = Gaming" +
                                                            "tags = escapefromtarkov, eft, dahmien7" +
                                                            "thumbnails = https://i.ytimg.com/vi/Oru2Sebkm-s/default.jpg" +
                                                            "idSearch = 1" +
                                                            "score = 30.0" +
                                                        "Video :" +
                                                            "urlVideo = https://www.youtube.com/watch?v=G3ymkcMZVUw" +
                                                            "title = BOIRE OU S&#39;ENFUIR IL FAUT CHOISIR..." +
                                                            "channel = dahmien7" +
                                                            "category = Gaming" +
                                                            "tags = escapefromtarkov, eft, dahmien7" +
                                                            "thumbnails = https://i.ytimg.com/vi/G3ymkcMZVUw/default.jpg" +
                                                            "idSearch = 1" +
                                                            "score = 30.0" +
                                                        "Video :" +
                                                            "urlVideo = https://www.youtube.com/watch?v=hcza4iLCdUc" +
                                                            "title = NOUVELLE ARME : LE MOSIN !!" +
                                                            "        channel = dahmien7" +
                                                            "category =" +
                                                            "        tags = pubg, dahmien7" +
                                                            "thumbnails = https://i.ytimg.com/vi/hcza4iLCdUc/default.jpg" +
                                                            "idSearch = 1" +
                                                            "score = 10.0" +
                                                        "Video :" +
                                                            "urlVideo = https://www.youtube.com/watch?v=kBg2IrLhzMg" +
                                                            "title = NEW MAP PUBG VIKENDI - dahmien7" +
                                                            "channel = dahmien7" +
                                                            "category = Gaming" +
                                                            "tags = PUBG, NEW, MAP, VIKENDI, FR" +
                                                            "thumbnails = https://i.ytimg.com/vi/kBg2IrLhzMg/default.jpg" +
                                                            "idSearch = 1" +
                                                            "score = 20.0" +
                                                        "Video :" +
                                                            "urlVideo = https://www.youtube.com/watch?v=qjqcacsyeks" +
                                                            "title = LES RAISONS DE MON BAN..." +
                                                            "channel = dahmien7" +
                                                            "category =" +
                                                            "        tags = EFT, escapefromtarkov, dahmien7" +
                                                            "thumbnails = https://i.ytimg.com/vi/qjqcacsyeks/default.jpg" +
                                                            "idSearch = 1" +
                                                            "score = 20.0" +
                                                        "Video :" +
                                                            "urlVideo = https://www.youtube.com/watch?v=RtnOLmdFBPc" +
                                                            "title = HIGHLIGHTS #100 - dahmien7" +
                                                            "channel = dahmien7" +
                                                            "category =" +
                                                            "        tags = dahmien7" +
                                                            "thumbnails = https://i.ytimg.com/vi/RtnOLmdFBPc/default.jpg" +
                                                            "idSearch = 1" +
                                                            "score = 10.0" +
                                                        "Video :" +
                                                            "urlVideo = https://www.youtube.com/watch?v=m6Y_a3aHhgI" +
                                                            "title = MON RETOUR DANS L&#39;ESPORT ??" +
                                                            "channel = dahmien7" +
                                                            "category = Gaming" +
                                                            "tags = pubg, dahmien7" +
                                                            "thumbnails = https://i.ytimg.com/vi/m6Y_a3aHhgI/default.jpg" +
                                                            "idSearch = 1" +
                                                            "score = 20.0" +
                                                        "Video :" +
                                                            "urlVideo = https://www.youtube.com/watch?v=u3WK1jvFJYo" +
                                                            "title = PLAN Q SUR LAB !!" +
                                                            "        channel = dahmien7" +
                                                            "category = Gaming" +
                                                            "tags = escapefromtarkov, eft, dahmien7" +
                                                            "thumbnails = https://i.ytimg.com/vi/u3WK1jvFJYo/default.jpg" +
                                                            "idSearch = 1" +
                                                            "score = 30.0",
                                                        RECEIVE_QUEUE);
        assertEquals(1, azureQueue.getQueueSize(RECEIVE_QUEUE));
    }
}
