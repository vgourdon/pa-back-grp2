package esgi.pa.webscraping.azure;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.queue.CloudQueue;
import com.microsoft.azure.storage.queue.CloudQueueClient;
import com.microsoft.azure.storage.queue.CloudQueueMessage;
import esgi.pa.webscraping.shared.Utils;
import esgi.pa.webscraping.models.Search;

import java.util.ArrayList;

public class AzureQueue {
    public static final String STORAGE_CONNECTION_STRING = System.getenv("AZURE_KEY");

    public static Search getAzureMessage(String receivedQueue) {
        try
        {
            // Retrieve storage account from connection-string.
            CloudStorageAccount storageAccount =
                    CloudStorageAccount.parse(STORAGE_CONNECTION_STRING);

            // Create the queue client.
            CloudQueueClient queueClient = storageAccount.createCloudQueueClient();

            // Retrieve a reference to a queue.
            CloudQueue queue = queueClient.getQueueReference(receivedQueue);

            // Retrieve the first visible message in the queue and delete it after.
            CloudQueueMessage retrievedMessage = queue.retrieveMessage();
            if (retrievedMessage != null)
            {
                Search search = Utils.stringToSearch(retrievedMessage.getMessageContentAsString());
                queue.deleteMessage(retrievedMessage);
                return search;
            }
        }
        catch (Exception e)
        {
            // Output the stack trace.
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<String> getAllQueues() {
        ArrayList<String> queues = new ArrayList<>();
        try
        {
            // Retrieve storage account from connection-string.
            CloudStorageAccount storageAccount =
                    CloudStorageAccount.parse(STORAGE_CONNECTION_STRING);

            // Create the queue client.
            CloudQueueClient queueClient =
                    storageAccount.createCloudQueueClient();

            // Loop through the collection of queues.
            for (CloudQueue queue : queueClient.listQueues())
            {
                queues.add(queue.getName());
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return queues;
    }
}