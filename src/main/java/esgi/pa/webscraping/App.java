package esgi.pa.webscraping;

import esgi.pa.webscraping.models.Result;
import esgi.pa.webscraping.models.Search;
import esgi.pa.webscraping.models.Video;
import esgi.pa.webscraping.threads.GetAzureMessageThread;
import esgi.pa.webscraping.threads.SearchVideoThread;
import esgi.pa.webscraping.threads.SendAzureMessageThread;

import java.util.*;

public class App
{
    public static void main( String[] args ) {
        ArrayList<Search> searchQueue = new ArrayList<>();              // List with all the video to search and their parameters (author, categories ...)
        ArrayList<Video> videoQueue = new ArrayList<>();                // List with all the video to search and their information
        ArrayList<String> azureReceiveQueuesList = new ArrayList<>();   // List of all the Receive Azure Queues
        ArrayList<Video> resultVideoList = new ArrayList<>();           // List of all the results for each video to search (associated to the username queue)
        ArrayList<Result> returnResultList = new ArrayList<>();         // List of all the result to send to the queue

        Process process = new Process(azureReceiveQueuesList,
                searchQueue,
                videoQueue,
                                        resultVideoList,
                returnResultList);

        // THREAD 1 - Get the messages on the queues
        GetAzureMessageThread getAzureMessageThread = new GetAzureMessageThread(process);
        getAzureMessageThread.start();

        // THREAD 2 - Search the videos for each request
        SearchVideoThread searchVideoThread = new SearchVideoThread(process);
        searchVideoThread.start();


        // THREAD 3 - Send the result to the server
        SendAzureMessageThread sendAzureMessageThread = new SendAzureMessageThread(process);
        sendAzureMessageThread.start();

        System.out.println("App launched.");
    }
}
