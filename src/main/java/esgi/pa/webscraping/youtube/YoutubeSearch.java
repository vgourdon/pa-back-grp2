package esgi.pa.webscraping.youtube;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.*;
import esgi.pa.webscraping.models.Search;
import esgi.pa.webscraping.models.Video;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class YoutubeSearch {
    /**
     * Define a global variable that identifies the name of a file that
     * contains the developer's API key.
     */

    private static final long NUMBER_OF_VIDEOS_RETURNED = 10;

    /**
     * Get infos from an Youtube URL. If param video == null : full search of the video;
     *                                else : Search only the categories and tags of this video (not provided by the API)
     * @param youtubeUrl : Youtube URL
     * @param search : Search
     * @param video : Video
     * @param fullInfos : Get all the infos or just tags and categories
     * @return Video
     */
    public static Video getInfosForYTVideo(String youtubeUrl, Search search, Video video, boolean fullInfos) {
        Video foundVideo = video;
        URL url;
        InputStream is = null;
        BufferedReader br;
        String line;
        StringBuilder titleVideo = new StringBuilder();
        StringBuilder channelVideo = new StringBuilder();
        StringBuilder thumbnailsVideo = new StringBuilder();
        StringBuilder tagsVideo = new StringBuilder();
        StringBuilder categoriesVideo = new StringBuilder();

        ArrayList<String> tagsArray = new ArrayList<>();
        ArrayList<String> categoriesArray = new ArrayList<>();

        try {
            url = new URL(youtubeUrl);
            is = url.openStream();
            br = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));

            // Read all the Youtube page source code
            while ((line = br.readLine()) != null) {
                if(fullInfos) {
                    if (line.contains("\"og:title\"")) {         // Get the title
                        titleVideo.append(line);
                    }
                    if (line.contains("\"name\":")) {            // Get the channel
                        channelVideo.append(line);
                    }
                    if (line.contains("\"og:image\"")) {         // Get the thumbnails url
                        thumbnailsVideo.append(line);
                    }
                }

                if(line.contains("\"og:video:tag\"")) {     // Get the tags
                    tagsArray.add(line);
                }

                if(line.contains("itemprop=\"genre\"")) {   // Get the category
                    categoriesArray.add(line);
                }
            }

            if(fullInfos) {
                // Extract the title
                if (titleVideo.length() != 0) {
                    int index = titleVideo.indexOf("content=\"");
                    titleVideo = new StringBuilder(titleVideo.substring(index + 9, titleVideo.length() - 2));
                }

                // Extract the channel
                if (channelVideo.length() != 0) {
                    int index = channelVideo.indexOf("\"name\": \"");
                    channelVideo = new StringBuilder(channelVideo.substring(index + 9, channelVideo.length() - 1));
                    if (channelVideo.length() > 50)
                        channelVideo = new StringBuilder();
                }

                // Extract the thumbnails url
                if (thumbnailsVideo.length() != 0) {
                    int index = thumbnailsVideo.indexOf("content=\"");
                    thumbnailsVideo = new StringBuilder(thumbnailsVideo.substring(index + 9, thumbnailsVideo.length() - 2));

                }
            }

            // Extract the tags, limit to 5 tags max
            if(!tagsArray.isEmpty()){
                int max = tagsArray.size();
                if(tagsArray.size() > 5)
                    max = 5;
                for (int i = 0; i < max; i++) {
                    String s = tagsArray.get(i);
                    s = extractText(s);
                    if(i != max - 1)
                        tagsVideo.append(s).append(", ");
                    else
                        tagsVideo.append(s);
                }
            }
            if(!fullInfos)
                foundVideo.setTags(tagsVideo.toString());

            // Extract the category
            if(!categoriesArray.isEmpty()) {
                for (int i = 0; i < categoriesArray.size(); i++) {
                    String s = categoriesArray.get(i);
                    s = extractText(s);
                    if(i != categoriesArray.size() - 1)
                        categoriesVideo.append(s).append(", ");
                    else
                        categoriesVideo.append(s);
                }
            }
            if(!fullInfos) {
                foundVideo.setCategories(categoriesVideo.toString());
                // Replace special characters
                if(foundVideo.getTitle().contains("&amp;") || foundVideo.getTitle().contains("&#39;"))
                    foundVideo.setTitle(replaceCharInString(foundVideo.getTitle()));
                if(foundVideo.getChannel().contains("&amp;") || foundVideo.getChannel().contains("&#39;"))
                    foundVideo.setChannel(replaceCharInString(foundVideo.getChannel()));

            } else {        // In case of search of a new Video
                foundVideo = new Video(new URL(youtubeUrl),
                        replaceCharInStringBuilder(titleVideo),
                        replaceCharInStringBuilder(channelVideo),
                        categoriesVideo.toString(),
                        tagsVideo.toString(),
                        new URL(thumbnailsVideo.toString()),
                        search);
            }

            return foundVideo;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (is != null) is.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
        return null;
    }

    public static ArrayList<Video> searchVideos(String searchParam, Search search) {
        if(searchParam.isEmpty())
            return new ArrayList<>();

        ArrayList<Video> foundVideo = new ArrayList<>();
        try {
            // This object is used to make YouTube Data API requests. The last
            // argument is required, but since we don't need anything
            // initialized when the HttpRequest is initialized, we override
            // the interface and provide a no-op function.
            /*
             * Define a global instance of a Youtube object, which will be used
             * to make YouTube Data API requests.
             */
            YouTube youtube = new YouTube.Builder(YoutubeAPIAuthentication.HTTP_TRANSPORT, YoutubeAPIAuthentication.JSON_FACTORY, request -> {
            }).setApplicationName("youtube-cmdline-search-sample").build();

            // Define the API request for retrieving search results.
            YouTube.Search.List searchParams = youtube.search().list("id,snippet");

            // Set your developer key from the {{ Google Cloud Console }}.
            searchParams.setKey(System.getenv("YOUTUBE_API_KEY"));
            searchParams.setQ(searchParam);

            // Restrict the search results to only include videos.
            // Possible parameters : "channel", "playlist", "video"
            searchParams.setType("video");

            // To increase efficiency, only retrieve the fields that the
            // application uses.
            searchParams.setFields("items(id/kind," +
                                    "id/videoId," +
                                    "snippet/title," +
                                    "snippet/channelTitle," +
                                    "snippet/thumbnails/default/url)");
            searchParams.setMaxResults(NUMBER_OF_VIDEOS_RETURNED);

            // Call the API and print results.
            SearchListResponse searchResponse = searchParams.execute();
            List<SearchResult> searchResultList = searchResponse.getItems();
            if (searchResultList != null) {
                for (SearchResult singleVideo : searchResultList) {
                    ResourceId rId = singleVideo.getId();

                    // Confirm that the result represents a video. Otherwise, don't add it
                    if (rId.getKind().equals("youtube#video")) {
                        Video video = new Video(new URL("https://www.youtube.com/watch?v=" + singleVideo.getId().getVideoId()),
                                singleVideo.getSnippet().getTitle(),
                                singleVideo.getSnippet().getChannelTitle(),
                                new URL(singleVideo.getSnippet().getThumbnails().getDefault().getUrl()),
                                search);

                        video = getInfosForYTVideo(video.getUrl(), search, video, false);
                        assert video != null;
                        video.setScore();

                        // Add only if it's score is relevant and it is not the same as the one researched
                        if (video.getScore() != 0.0 && !video.getUrl().equals(video.getSearch().getUrlVideo()))
                            foundVideo.add(video);
                    }
                }
            }
            return foundVideo;
        } catch (GoogleJsonResponseException e) {
            System.err.println("There was a service error: " + e.getDetails().getCode() + " : "
                    + e.getDetails().getMessage());
        } catch (IOException e) {
            System.err.println("There was an IO error: " + e.getCause() + " : " + e.getMessage());
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return null;
    }

    private static String extractText(String s) {
        int index = s.indexOf("content=\"");
        String substring = s.substring(index + 9, s.length() - 2);
        return replaceCharInString(substring);
    }

    private static String replaceCharInStringBuilder(StringBuilder stringBuilder) {
        return replaceCharInString(stringBuilder.toString());
    }

    private static String replaceCharInString(String tmpString) {
        if(tmpString.contains("&amp;"))
            tmpString = tmpString.replaceAll("&amp;", "&");
        if(tmpString.contains("&#39;"))
            tmpString = tmpString.replaceAll("&#39;", "'");
        return tmpString;
    }
}
