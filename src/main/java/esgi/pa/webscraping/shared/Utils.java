package esgi.pa.webscraping.shared;

import esgi.pa.webscraping.models.Search;
import esgi.pa.webscraping.models.Video;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;

public class Utils {
    public enum States {
        STEP_10,
        STEP_20,
        STEP_30,
        STEP_50,
        STEP_70,
        STEP_80,
        STEP_90,
        STEP_100
    }

    public static Search stringToSearch(String message) {
        ObjectMapper mapper = new ObjectMapper();
        Search search = null;
        try
        {
            search =  mapper.readValue(message, Search.class);
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return search;
    }

    public static void addAllIfDifferent(ArrayList<Video> base, ArrayList<Video> add) {
        boolean toAdd = true;
        for(Video videoAdd : add) {
            for(Video videoBase : base) {
                if (videoAdd.equals(videoBase)) {
                    toAdd = false;
                    break;
                }
            }
            if(toAdd)
                base.add(videoAdd);
            toAdd = true;
        }
    }

    public static Double getScoreFormTwoTexts(String base, String toCompare, double points) {
        double score = 0.0;
        if(base == null || toCompare == null)
            return score;
        ArrayList<String> baseArray = stringToArray(base);
        ArrayList<String> toCompareArray = stringToArray(toCompare);

        for(String baseFor : baseArray) {
            for(String toCompareFor : toCompareArray) {
                if(baseFor.equalsIgnoreCase(toCompareFor))
                    score += points;
            }
        }

        return score;
    }

    private static ArrayList<String> stringToArray(String text) {
        ArrayList<String> textArray = new ArrayList<>();
        StringBuilder tmpWord = new StringBuilder();

        for(int i = 0; i < text.length(); i++) {
            if(text.charAt(i) == ' ') {
                if(tmpWord.length() > 1)                    // If it's just a letter, ignore it.
                    textArray.add(tmpWord.toString());
                tmpWord = new StringBuilder();
            } else
                tmpWord.append(text.charAt(i));
        }
        if(tmpWord.length() > 1)
            textArray.add(tmpWord.toString());

        return textArray;
    }

    public static Integer stateToInt(Utils.States idState) {
        switch(idState) {
            case STEP_10:
                return 10;
            case STEP_20:
                return 20;
            case STEP_30:
                return 30;
            case STEP_50:
                return 50;
            case STEP_70:
                return 70;
            case STEP_80:
                return 80;
            case STEP_90:
                return 90;
            case STEP_100:
                return 100;
            default:
                return 0;
        }
    }
}
