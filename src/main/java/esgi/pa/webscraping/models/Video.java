package esgi.pa.webscraping.models;

import esgi.pa.webscraping.shared.Utils;

import java.io.Serializable;
import java.net.URL;
import java.util.Objects;

public class Video implements Serializable, Comparable<Video> {
    private URL url;
    private String title;
    private String channel;
    private String categories;
    private String tags;
    private URL thumbnails;
    private Search search;
    private Double score;

    public Video(URL url, String title, String channel, URL thumbnails, Search search) {
        this.url = url;
        this.title = title;
        this.channel = channel;
        this.categories = "";
        this.tags = "";
        this.thumbnails = thumbnails;
        this.search = search;
        this.score = 0.0;
    }

    public Video(URL url, String title, String channel, String categories, String tags, URL thumbnails, Search search) {
        this.url = url;
        this.title = title;
        this.channel = channel;
        this.categories = categories;
        this.tags = tags;
        this.thumbnails = thumbnails;
        this.search = search;
        this.score = 0.0;
    }

    public String getUrl() {
        return url.toString();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Search getSearch() {
        return search;
    }

    public Double getScore() {
        return score;
    }

    /** Calculate the video's score */
    public void setScore() {
        double score = 0.0;

        // Calculate for the Channel name
        if(this.channel.equalsIgnoreCase(this.search.getYoutuberName()))
            score += 10.0;

        // Calculate for the Categories
        score += Utils.getScoreFormTwoTexts(this.categories, this.getSearch().getCategoryVideo(), 10.0);

        // Calculate for the Tags
        score += Utils.getScoreFormTwoTexts(this.tags, this.getSearch().getKeyWords(), 10.0);

        // Calculate for the Titles
        score += Utils.getScoreFormTwoTexts(this.title, this.getSearch().getTitle(), 5.0);

        this.score = score;
    }

    @Override
    public String toString() {
        return "Video :" +
                "\n\turlVideo = " + url.toString() +
                "\n\ttitle = " + title +
                "\n\tchannel = " + channel +
                "\n\tcategory = " + categories +
                "\n\ttags = " + tags +
                "\n\tthumbnails = " + thumbnails +
                "\n\tidSearch = " + this.search.getIdSearch() +
                "\n\tscore = " + this.score;
    }

    public String toJSON() {
        return "\"urlVideo\":\"" + url.toString() + "\"" +
                ",\"categoryVideo\":\"" + categories + "\"" +
                ",\"youtuberName\":\"" + channel + "\"" +
                ",\"keyWords\":\"" + tags + "\"" +
                ",\"urlVignette\":\"" + this.thumbnails + "\"";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Video)) return false;
        Video video = (Video) o;
        return  Objects.equals(title, video.title) &&
                Objects.equals(channel, video.channel);
    }

    @Override
    public int compareTo(Video video) {
        return video.getScore().compareTo(this.getScore());
    }
}
