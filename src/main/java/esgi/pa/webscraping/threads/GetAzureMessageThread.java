package esgi.pa.webscraping.threads;

import esgi.pa.webscraping.Process;

import java.io.IOException;

public class GetAzureMessageThread extends Thread {

    Process process;

    public GetAzureMessageThread(Process process) {
        this.process = process;
    }

    public void run() {
        try {
            this.process.getAzureMessage();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
