package esgi.pa.webscraping.server;

import esgi.pa.webscraping.models.Result;
import esgi.pa.webscraping.models.StateSearch;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class Communication {
    public static boolean sendToServer(Object o) throws IOException {
        if(o instanceof StateSearch || o instanceof Result) {
            String token = signIn();
            if (token == null) {
                System.out.println("Unable to sign in.");
                return false;
            }

            String jsonInputString;
            URL url;

            if (o.getClass() == StateSearch.class) {
                url = new URL(System.getenv("SERVER_URL") + System.getenv("SERVER_ROUTE_POSTSTATE"));
                StateSearch stateSearch = (StateSearch) o;
                jsonInputString = stateSearch.toJSON();
            } else {
                url = new URL(System.getenv("SERVER_URL") + System.getenv("SERVER_ROUTE_POSTRESULT"));
                assert o instanceof Result;
                Result result = (Result) o;
                jsonInputString = result.toJSON();
            }

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            System.out.println("Sending :\n" + jsonInputString);
            conn.setRequestMethod("POST");

            // Set "Authorization - Bearer" request header to use a token
            conn.setRequestProperty("Authorization", "Bearer " + token);

            // Set “content-type” request header to “application/json” to send the request content in JSON form.
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

            // Set the “Accept” request header to “application/json” to read the response in the desired format
            conn.setRequestProperty("Accept", "application/json");

            // Ensure the connection will be used to send content
            conn.setDoOutput(true);


            try (OutputStream os = conn.getOutputStream()) {
                byte[] input = jsonInputString.getBytes(StandardCharsets.UTF_8);
                os.write(input, 0, input.length);
            }

            // Stop process if response code is 500
            if(conn.getResponseCode() == 500) {
                System.out.println("ERROR while sending the message. Response code : " + conn.getResponseCode());
                return false;
            }
            else
                conn.getInputStream();
            return true;
        }
        return false;
    }

    private static String signIn() throws IOException {
        String url = System.getenv("SERVER_URL") + System.getenv("SERVER_ROUTE_SIGNIN");
        URL obj = new URL(url);

        HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
        conn.setRequestMethod("POST");

        // Set “content-type” request header to “application/json” to send the request content in JSON form.
        conn.setRequestProperty("Content-Type", "application/json; utf-8");

        // Set the “Accept” request header to “application/json” to read the response in the desired format
        conn.setRequestProperty("Accept", "application/json");

        // Ensure the connection will be used to send content
        conn.setDoOutput(true);

        String jsonInputString = "{" +
                "\"username\": \"" + System.getenv("SERVER_LOGIN") + "\"," +
                "\"password\": \"" + System.getenv("SERVER_PWD") + "\"" +
                "}";

        try(OutputStream os = conn.getOutputStream()) {
            byte[] input = jsonInputString.getBytes(StandardCharsets.UTF_8);
            os.write(input, 0, input.length);
        }

        // Continue only if response code ok
        if(conn.getResponseCode() != 200)
            return null;

        InputStream is = conn.getInputStream();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(is));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //Read JSON response and print
        JSONObject myResponse = new JSONObject(response.toString());

        return myResponse.getString("accessToken");
    }
}
